package ch.ost.rj.mge.warrantytracker.services;

import android.content.Context;
import android.graphics.Bitmap;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class ImageService {

    public static void writeImage(String uuid, Bitmap picture, Context context){
        File file = new File(context.getFilesDir(), uuid + ".jpg");
        try {
            FileOutputStream out = new FileOutputStream(file);
            picture.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Bitmap readImage(String uuid, Context context){
        File file = new File(context.getFilesDir(), uuid + ".jpg");
        return android.graphics.BitmapFactory.decodeFile(file.getAbsolutePath());
    }
}


