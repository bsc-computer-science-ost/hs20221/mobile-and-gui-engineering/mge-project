package ch.ost.rj.mge.warrantytracker.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

@Entity
public class PersistentWarrantyItem {
    @PrimaryKey
    @NotNull
    public String uuid;

    @ColumnInfo
    public String productName;

    @ColumnInfo
    public String vendor;

    @ColumnInfo
    public Long buyDate;

    @ColumnInfo
    public Long expirationDate;


    public PersistentWarrantyItem(@NonNull String uuid, String productName, String vendor, Long buyDate, Long expirationDate) {
        this.uuid = uuid;
        this.productName = productName;
        this.vendor = vendor;
        this.buyDate = buyDate;
        this.expirationDate = expirationDate;
    }
}
