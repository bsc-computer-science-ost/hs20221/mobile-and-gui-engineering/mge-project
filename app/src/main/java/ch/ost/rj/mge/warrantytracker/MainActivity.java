package ch.ost.rj.mge.warrantytracker;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.widget.Button;

import ch.ost.rj.mge.warrantytracker.services.ImageService;
import ch.ost.rj.mge.warrantytracker.services.ItemsService;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button addItemButton = findViewById(R.id.main_additem);
        Button showItemsButton = findViewById(R.id.show_item);

        new ItemsService(getApplicationContext());
        new ImageService();

        addItemButton.setOnClickListener(v -> startActionMode(AddItemActivity.class));
        showItemsButton.setOnClickListener(v -> startActionMode(ItemsOverviewActivity.class));
    }

    private void startActionMode(Class<?> cls) {
        Intent intent = new Intent(this, cls);
        startActivity(intent);
    }


}