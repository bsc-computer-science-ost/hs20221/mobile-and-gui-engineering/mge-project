package ch.ost.rj.mge.warrantytracker.services;

import android.content.Context;
import android.os.Vibrator;

public class VibrationService {
    private static Vibrator vibrator;

    public static void initialize(Context context) {
        vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
    }

    public static void vibrate() {
        vibrator.vibrate(100);
    }
}
