package ch.ost.rj.mge.warrantytracker.model;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface PersistentWarrantyItemDao {
    @Query("SELECT * FROM PersistentWarrantyItem")
    List<PersistentWarrantyItem> getAll();

    @Query("SELECT * FROM PersistentWarrantyItem WHERE uuid = :uuid")
    PersistentWarrantyItem getItemByUUID(String uuid);

    @Insert
    void insertItem(PersistentWarrantyItem item);

    @Update
    void updateItem(PersistentWarrantyItem item);

   @Delete
    void deleteItem(PersistentWarrantyItem item);

}
