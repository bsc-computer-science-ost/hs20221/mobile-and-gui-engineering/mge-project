package ch.ost.rj.mge.warrantytracker.model;

import java.util.Date;
import java.util.UUID;


public class WarrantyItem {
    private final String uuid;
    private String productName;
    private String vendor;
    private Date buyDate;
    private Date expirationDate;

    public WarrantyItem(String productName, String vendor, Date buyDate, Date expirationDate){
        this.uuid = UUID.randomUUID().toString();
        this.productName = productName;
        this.vendor = vendor;
        this.buyDate = buyDate;
        this.expirationDate = expirationDate;
    }

    public WarrantyItem(PersistentWarrantyItem item) {
        this.uuid = item.uuid;
        this.productName = item.productName;
        this.vendor = item.vendor;
        this.buyDate = new Date(item.buyDate);
        this.expirationDate = new Date(item.expirationDate);
    }

    public String getProductName() {
        return this.productName;
    }
    public void setProductName(String productName){this.productName = productName;}

    public String getVendor() {
        return this.vendor;
    }
    public void setVendor(String vendor) { this.vendor = vendor; }

    public Date getExpirationDate() {
        return this.expirationDate;
    }
    public void setExpirationDate(Date expirationDate) { this.expirationDate = expirationDate; }

    public Date getBuyDate() {
        return this.buyDate;
    }
    public void setBuyDate(Date buyDate) { this.buyDate = buyDate; }

    public String getUUID(){ return this.uuid;}
}
