package ch.ost.rj.mge.warrantytracker.services;

import android.content.Context;
import androidx.room.Room;

import java.util.ArrayList;
import java.util.List;


import ch.ost.rj.mge.warrantytracker.model.AppDatabase;
import ch.ost.rj.mge.warrantytracker.model.PersistentWarrantyItem;
import ch.ost.rj.mge.warrantytracker.model.WarrantyItem;

public class ItemsService {
    private static AppDatabase db;

    public ItemsService(Context context) {
        db = Room.databaseBuilder(context, AppDatabase.class, "item-db").allowMainThreadQueries().build();
    }

    public static List<WarrantyItem> getItems() {
        List<PersistentWarrantyItem> persistentItems = db.PersistentWarrantyItemDao().getAll();

        List<WarrantyItem> items = new ArrayList<>();
        for (PersistentWarrantyItem item : persistentItems) {
            items.add(new WarrantyItem(item));
        }
        return items;
    }

    public static void addItem(WarrantyItem item) {
        db.PersistentWarrantyItemDao().insertItem(convertToPersistentWarrantyItem(item));
    }

    public static void updateItem(WarrantyItem item){
        db.PersistentWarrantyItemDao().updateItem(convertToPersistentWarrantyItem(item));
    }

    public static void deleteItem(WarrantyItem item){
        db.PersistentWarrantyItemDao().deleteItem(convertToPersistentWarrantyItem(item));
    }

    public static WarrantyItem getItemByUUID(String uuid){
        return new WarrantyItem(db.PersistentWarrantyItemDao().getItemByUUID(uuid));
    }

    private static PersistentWarrantyItem convertToPersistentWarrantyItem(WarrantyItem item){
        return new PersistentWarrantyItem(
                item.getUUID(),
                item.getProductName(),
                item.getVendor(),
                item.getBuyDate().getTime(),
                item.getExpirationDate().getTime()
        );
    }
}
