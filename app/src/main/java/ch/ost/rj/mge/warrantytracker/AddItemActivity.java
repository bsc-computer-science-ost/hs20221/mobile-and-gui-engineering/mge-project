package ch.ost.rj.mge.warrantytracker;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.Image;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import ch.ost.rj.mge.warrantytracker.model.WarrantyItem;
import ch.ost.rj.mge.warrantytracker.services.ImageService;
import ch.ost.rj.mge.warrantytracker.services.ItemsService;
import ch.ost.rj.mge.warrantytracker.services.VibrationService;

public class AddItemActivity extends AppCompatActivity {
    static final int REQUEST_IMAGE_CAPTURE = 1;

    final Calendar expirationDate = Calendar.getInstance();
    private EditText itemNameText;
    private EditText itemVendorText;
    private EditText itemExpirationText;
    private EditText itemBuyText;
    private ImageView itemPicture;
    private String uuid = null;

    private FloatingActionButton buttonAddItem;
    private Button buttonAddImage;

    private VibrationService vibrationService;

    public AddItemActivity() { }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            itemPicture.setImageBitmap(imageBitmap);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_IMAGE_CAPTURE) {
            takePicture();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);

        vibrationService  = new VibrationService();
        vibrationService.initialize(getApplicationContext());

        itemNameText = findViewById(R.id.additem_product_name);
        itemVendorText = findViewById(R.id.additem_vendor_name);
        itemExpirationText = findViewById(R.id.additem_expiration_date);
        itemBuyText = findViewById(R.id.additem_buy_date);
        itemPicture = findViewById(R.id.additem_imageview_item);

        DatePickerDialog.OnDateSetListener expDate = (view, year, month, day) -> {
            expirationDate.set(Calendar.YEAR, year);
            expirationDate.set(Calendar.MONTH, month);
            expirationDate.set(Calendar.DAY_OF_MONTH, day);
            updateLabel(itemExpirationText);
        };
        itemExpirationText.setOnClickListener(view -> new DatePickerDialog(AddItemActivity.this, expDate, expirationDate.get(Calendar.YEAR), expirationDate.get(Calendar.MONTH), expirationDate.get(Calendar.DAY_OF_MONTH)).show());

        DatePickerDialog.OnDateSetListener buyDate = (view, year, month, day) -> {
            expirationDate.set(Calendar.YEAR, year);
            expirationDate.set(Calendar.MONTH, month);
            expirationDate.set(Calendar.DAY_OF_MONTH, day);
            updateLabel(itemBuyText);
        };
        itemBuyText.setOnClickListener(view -> new DatePickerDialog(AddItemActivity.this, buyDate, expirationDate.get(Calendar.YEAR), expirationDate.get(Calendar.MONTH), expirationDate.get(Calendar.DAY_OF_MONTH)).show());

        buttonAddImage = findViewById(R.id.additem_button_addimage);
        buttonAddImage.setOnClickListener(v -> takePicture());

        buttonAddItem = findViewById(R.id.additem_button_add);
        buttonAddItem.setOnClickListener(v-> {
            if (checkInputFields()){ addItem(uuid); }
        });

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            uuid = (String) bundle.get("uuid");
            WarrantyItem item = ItemsService.getItemByUUID(uuid);
            itemNameText.setText(item.getProductName());
            itemVendorText.setText(item.getVendor());
            itemBuyText.setText(DateFormat.getDateInstance(DateFormat.SHORT, Locale.FRANCE).format(item.getBuyDate()));
            itemExpirationText.setText(DateFormat.getDateInstance(DateFormat.SHORT, Locale.FRANCE).format(item.getExpirationDate()));
            itemPicture.setImageBitmap(ImageService.readImage(item.getUUID(), getApplicationContext()));
        }
    }

    private void updateLabel(EditText editText) {
        String myFormat = "dd/MM/yyyy";
        SimpleDateFormat dateFormat = new SimpleDateFormat(myFormat, Locale.FRENCH);
        editText.setText(dateFormat.format(expirationDate.getTime()));
    }

    private void addItem(String uuid) {
        SimpleDateFormat simpleDate = new SimpleDateFormat("dd/MM/yyyy");
        itemPicture.buildDrawingCache();
        Bitmap picture = itemPicture.getDrawingCache();

        try {
            if (uuid != null){
                WarrantyItem item = ItemsService.getItemByUUID(uuid);
                item.setProductName(itemNameText.getText().toString());
                item.setVendor(itemVendorText.getText().toString());
                item.setExpirationDate(simpleDate.parse(itemExpirationText.getText().toString()));
                item.setBuyDate(simpleDate.parse(itemBuyText.getText().toString()));

                ImageService.writeImage(item.getUUID(), picture, getApplicationContext());
                ItemsService.updateItem(item);
                Toast.makeText(this, "Item updated", Toast.LENGTH_SHORT).show();
                vibrationService.vibrate();
            } else {
                WarrantyItem item = new WarrantyItem(
                        itemNameText.getText().toString(),
                        itemVendorText.getText().toString(),
                        simpleDate.parse(itemBuyText.getText().toString()),
                        simpleDate.parse(itemExpirationText.getText().toString()));

                ItemsService.addItem(item);
                ImageService.writeImage(item.getUUID(), picture, getApplicationContext());
                Toast.makeText(this, "Item added", Toast.LENGTH_SHORT).show();
                vibrationService.vibrate();
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        finish();
        Intent intent = new Intent(this, ItemsOverviewActivity.class);
        startActivity(intent);
    }

    private boolean checkInputFields(){
        if(itemNameText.length() == 0){
            itemNameText.setError("Product name is required");
            return false;
        }
        if(itemVendorText.length() == 0){
            itemVendorText.setError("Product vendor is required");
            return false;
        }
        if(itemExpirationText.length() == 0){
            itemExpirationText.setError("Expiration date is required");
            return false;
        } else if(itemExpirationText.length() < 8){
            itemExpirationText.setError("Expiration date is not valid");
            return false;
        }
        if(itemBuyText.length() == 0){
            itemBuyText.setError("Buy date is required");
            return false;
        } else if(itemBuyText.length() < 8){
            itemBuyText.setError("Buy date is not valid");
            return false;
        }
        return true;
    }

    private boolean checkCameraPermissions() {
        return requestPermission(Manifest.permission.CAMERA, REQUEST_IMAGE_CAPTURE);
    }

    private boolean requestPermission(String permission, int requestCode) {
        if(checkPermission(permission)) {
            return true;
        } else {
            requestPermissions(new String[] { permission }, requestCode);
            return false;
        }
    }

    private void takePicture() {
        if (checkCameraPermissions()) {
            try {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(this, "Error while Starting Camera", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean checkPermission(String permission) {
        return ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED;
    }

}
