package ch.ost.rj.mge.warrantytracker.model;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {PersistentWarrantyItem.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract PersistentWarrantyItemDao PersistentWarrantyItemDao();
}