package ch.ost.rj.mge.warrantytracker;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;

import com.google.android.material.snackbar.Snackbar;

import java.util.Comparator;
import java.util.List;

import ch.ost.rj.mge.warrantytracker.adapter.WarrantyItemAdapter;
import ch.ost.rj.mge.warrantytracker.model.WarrantyItem;
import ch.ost.rj.mge.warrantytracker.services.ItemsService;

public class ItemsOverviewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        List<WarrantyItem> data = ItemsService.getItems();
        data.sort(Comparator.comparing(WarrantyItem::getExpirationDate));

        if (data.isEmpty()) {
            setContentView(R.layout.activity_empty);
        } else {
            setContentView(R.layout.activity_items_overview);

            RecyclerView recyclerView = findViewById(R.id.overview_items);

            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(layoutManager);

            WarrantyItemAdapter adapter = new WarrantyItemAdapter(data);
            recyclerView.setAdapter(adapter);

            new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
                @Override
                public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                    return false;
                }

                @Override
                public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                    int position = viewHolder.getAdapterPosition();
                    WarrantyItem deletedItem = data.get(position);
                    data.remove(position);
                    ItemsService.deleteItem(deletedItem);
                    adapter.notifyItemRemoved(position);

                    CountDownTimer counterTimer = new CountDownTimer(3000, 1000) {
                        @Override
                        public void onTick(long l) {
                        }

                        @Override
                        public void onFinish() {
                            finish();
                            overridePendingTransition(0, 0);
                            startActivity(getIntent());
                            overridePendingTransition(0, 0);
                        }
                    };
                    counterTimer.start();

                    Snackbar.make(recyclerView, "'" + deletedItem.getProductName() + "' deleted", Snackbar.LENGTH_LONG).setAction("Undo", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            counterTimer.cancel();
                            data.add(position, deletedItem);
                            ItemsService.addItem(deletedItem);
                            adapter.notifyItemInserted(position);
                        }
                    }).show();
                }
            }).attachToRecyclerView(recyclerView);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        finish();
    }
}