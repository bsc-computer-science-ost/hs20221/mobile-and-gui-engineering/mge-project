package ch.ost.rj.mge.warrantytracker.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.sql.Date;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.List;

import ch.ost.rj.mge.warrantytracker.AddItemActivity;
import ch.ost.rj.mge.warrantytracker.R;
import ch.ost.rj.mge.warrantytracker.model.WarrantyItem;

public class WarrantyItemAdapter extends RecyclerView.Adapter<WarrantyItemAdapter.ViewHolder> {
    private final List<WarrantyItem> items;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView itemNameTV;
        public TextView expirationDateTV;
        public LinearLayout itemCard;

        public ViewHolder(View parent){
            super(parent);
            itemNameTV = parent.findViewById(R.id.overviewProductName);
            expirationDateTV = parent.findViewById(R.id.overviewExpirationDate);
            itemCard = parent.findViewById(R.id.item_linearLayout);
        }
    }

    public WarrantyItemAdapter(List<WarrantyItem> items) {this.items = items;}

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(
                R.layout.card_layout,
                parent,
                false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        WarrantyItem item = this.items.get(position);
        holder.itemNameTV.setText(item.getProductName());
        holder.expirationDateTV.setText("Expires on " + DateFormat.getDateInstance(DateFormat.LONG).format(item.getExpirationDate()));
        long timDiff = item.getExpirationDate().getTime()-Calendar.getInstance().getTimeInMillis();
        if(0L < timDiff && timDiff < 604800000L){
            holder.itemCard.setBackgroundColor(ContextCompat.getColor(holder.itemCard.getContext(), R.color.expireWarningColor));
        }
        if(timDiff <= 0){
            holder.expirationDateTV.setText("Already expired on " + DateFormat.getDateInstance(DateFormat.LONG).format(item.getExpirationDate()));
            holder.itemCard.setBackgroundColor(ContextCompat.getColor(holder.itemCard.getContext(), R.color.pastExpireColor));
        }
        holder.itemView.setOnClickListener(view -> {
            Context context = view.getContext();
            Intent intent = new Intent(context, AddItemActivity.class);
            intent.putExtra("uuid", item.getUUID());
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return this.items.size();
    }
}
