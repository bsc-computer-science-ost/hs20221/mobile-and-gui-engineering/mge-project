# MGE Project

## Features to implement

### Functionality
- Make picture of waranty and save it in item
- Push notificatoin for item that the waranty is expiring

### Screens
- Overview Screen 
- Add Item Screen
- Details of Item Screen

### Persistency
- Save the items

## Documentation

## Evaluation

Since we are two team members we need to score at least 13 points.

| Feature | Points | Status
| ------ | ------ | --- |
| Show items overview | 2 | done |
| Add item functionality | 2 | done |
| Update item functionality | 1 | done |
| Delete item functionality & 'undo'-Snackbar | 1 | done |
| Persistency of items | 2 | done |
| Coloring of expiering items | 1 | done |
| Custom color themes | 2 | done |
| Vibration when saving items | 1 | done |
| App Icon | 1 | done |
| Use of Camera | 2 | done |
| Default image for empty recyclerView | 1 | done |
| Notification for expirations | 3 |  |
| Transaction and click animations | 2 |  |
||||

total done = 16 points


# Pictures

![Application Icon](images/icon.png)

![Main Screen](images/main-screen.png)

![Overview Screen](images/overview-screen.png)

![Add Item Screen](images/add-item-screen.png)
